using System.Collections.Generic;
using UnityEngine;
using Common.HexField.HexGrid;
using Common.HexField.HexGridObject;
using Common.HexField.HexList;
using Common.Utils;

namespace Project.Generators
{
    public class SimpleGenerator : MonoBehaviour
    {
        [SerializeField] private List<GridObject> _hexPrefabs;
        [SerializeField] private List<GridObject> _hexes;
        [SerializeField] private List<GridObject> _obstacles;
        private PointyGrid _grid;

        [SerializeField] private List<GridObject> _weightHexes;
        [SerializeField] private List<GridObject> _heightHexes;
        
        private GridObjectWeights _weights;
        private GridObjectHeights _heights;

        // [SerializeField] private HexObjectObstacles _obstacles;
        //private HexFieldPosition 
        private void Start()
        {
            //_field.Add(_hexes[0]);
            //_field.Add(_hexes[1]);
            //_field.Add(_hexes[2]);
            //_field.Add(_hexes[3]);
            _grid = new PointyGrid(_hexes);
            _weights = new GridObjectWeights();
            _weights.Add(_weightHexes[0], 1);
            _weights.Add(_weightHexes[1], 2);
            
            _heights = new GridObjectHeights();
            _heights.Add(_heightHexes[0], 0);
            _heights.Add(_heightHexes[1], GridObjectHeights.Infinite);
            _heights.Add(_heightHexes[2], GridObjectHeights.Infinite);

            // DebugUtils.LogList(_grid.GetLine(_hexes[0], _hexes[2], 15));
            DebugUtils.LogList(_grid.GetFieldOfView(_hexes[1], 5, 0, _heights));
            
            // _grid.GetAllObjects(_hexes[0], 4);
            // _weights.Add(new HexObjectWeight(_weightHexes[0], 1));
            // DebugUtils.LogList(_grid.GetPath(_hexes[0], _hexes[8], 4, _weights));
            // Debug.Log(_grid.GetPathTotalMovements(_hexes[0], _hexes[8], 4, _weights));
            // Debug.Log(_grid.GetPathWithMovements(_hexes[0], _hexes[8], 4, _weights));
            // DebugUtils.LogList(_grid.GetReachableObjects(_hexes[0], 3, _weights));
            // DebugUtils.LogList(_grid.GetAllObjectsInRange(_hexes[0], 3));
            // DebugUtils.LogList(_grid.GetReachableObjects(_hexes[0], 2, _obstacles, _weights));


            //_field.GetHexFieldElementsInRange(_hexes[0], 3, false);
        }
    }
}
