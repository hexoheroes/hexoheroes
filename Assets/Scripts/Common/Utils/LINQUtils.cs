using System.Collections.Generic;
using System.Linq;

namespace Common.Utils
{
    public static class LinqUtils
    {
        public static IEnumerable<(T Element1, T Element2)> GetAllPairs<T>(this IList<T> source)
        {
            //TODO i don't understand this)))
            return source.SelectMany((_, i) => source.Where((_, j) => i < j), (x, y) => (x, y));
        }

        public static IEnumerable<(TOne Element1, TTwo Element2)> GetAllPairs<TOne, TTwo>(IEnumerable<TOne> source, IList<TTwo> target)
        {
            return source.SelectMany(s => target.Select(t => (s, t)));
        }
        
        public static IEnumerable<T> FindKeysByValue<T, TU>(this Dictionary<T, TU> source, TU value)
        {
            return source.Where(pair => pair.Value.Equals(value)).Select(pair => pair.Key);
        }
    }
}