﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace Common.Tools.HexCreator
{
    public class HexCreator : EditorWindow
    {
        private string outerRadiusEdit = "5";
        private string hexPath = "Assets/Objects/Hex.mesh";

        [MenuItem("Create/Hex")]
        static void Init()
        {
            HexCreator window = (HexCreator)EditorWindow.GetWindow(typeof(HexCreator));
            window.titleContent.text = "Hex Creator";
            window.Show();
        }

        private void OnGUI()
        {
            GUILayout.Label("Hex Options", EditorStyles.boldLabel);

            GUI.Label(new Rect(3, 20, 100, 20), "Outer Radius");

            outerRadiusEdit = GUI.TextField(new Rect(103, 20, 30, 20), outerRadiusEdit);

            if (GUI.Button(new Rect(position.width - 123, 20, 120, 25), "Generate Hex"))
            {
                Mesh m = (Mesh)AssetDatabase.LoadAssetAtPath(hexPath, typeof(Mesh));

                if (m == null)
                {
                    m = new Mesh();
                }

                m.vertices = GetVertices();
                m.triangles = GetTriangles();
                m.RecalculateBounds();
                m.RecalculateNormals();
                m.RecalculateTangents();

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        //private Vector3[] GetVertices()
        //{
        //    //  1 ___ 2
        //    //  6/ 0 \3
        //    //   \___/
        //    //   5   4

        //    float outerRadius = float.Parse(outerRadiusEdit);
        //    //float innerRadius = Mathf.Sqrt(3) * outerRadius / 2;

        //    return new Vector3[] {
        //        new Vector3(0f, 0f, 0.0f), // point 0
        //        new Vector3(-outerRadius / 2, 0.0f, Mathf.Sqrt(3) / 2 * outerRadius), // point 1
        //        new Vector3(outerRadius / 2, 0.0f, Mathf.Sqrt(3) / 2 * outerRadius), // point 2
        //        new Vector3(outerRadius, 0.0f, 0.0f), // point 3
        //        new Vector3(outerRadius / 2, 0.0f, -Mathf.Sqrt(3) / 2 * outerRadius), // point 4
        //        new Vector3(-outerRadius / 2, 0.0f, -Mathf.Sqrt(3) / 2 * outerRadius), // point 5
        //        new Vector3(-outerRadius, 0.0f, 0.0f) // point 6
        //    };
        //}

        private Vector3[] GetVertices()
        {
            //  /1  \2
            // 6| 0 |3
            //  \5 4/
            //   

            float outerRadius = float.Parse(outerRadiusEdit);
            //float innerRadius = Mathf.Sqrt(3) * outerRadius / 2;

            return new Vector3[] {
                new Vector3(0f, 0f, 0.0f), // point 0
                new Vector3(0.0f, 0.0f, outerRadius), // point 1
                new Vector3(Mathf.Sqrt(3) / 2 * outerRadius, 0.0f, outerRadius / 2), // point 2
                new Vector3(Mathf.Sqrt(3) / 2 * outerRadius, 0.0f, -outerRadius / 2), // point 3
                new Vector3(0.0f, 0.0f, -outerRadius), // point 4
                new Vector3(Mathf.Sqrt(3) / 2 * -outerRadius, 0.0f, -outerRadius / 2), // point 5
                new Vector3(Mathf.Sqrt(3) / 2 * -outerRadius, 0.0f, outerRadius / 2) // point 6
            };
        }

        private int[] GetTriangles()
        {
            return new int[] { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 1 };
        }
    }
}
