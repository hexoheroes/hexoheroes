using System;

namespace Common.Utils
{
    public static class FloatUtils
    {
        public static float Round(this float value, int digits)
        {
            var rank = Math.Pow(10.0, digits);
            var result = Math.Truncate(rank * value) / rank;
            return (float)result;
        }

        public static bool EqualsApproximately(float a, float b, float tolerance)
        {
            return Math.Abs(a - b) < tolerance;
        }
    }
}