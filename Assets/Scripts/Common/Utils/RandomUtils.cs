﻿using System;

namespace Common.Utils
{
    public class RandomUtils
    {
        public bool RandomChance(float chance)
        {
            var rand = new Random();
            return rand.NextDouble() <= chance;
        }
    }
}
