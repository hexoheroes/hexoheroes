﻿
namespace Common.Utils
{
    public static class StringUtils
    {
        public static string CapitalizeFirstLetter(this string str)
        {
            return char.ToUpper(str[0]) + str.Substring(1);
        }
    }
}
