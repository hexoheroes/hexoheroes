using System;
using System.Collections.Generic;

namespace Common.Utils
{
    public static class ListUtils
    {
        public static T GetElementByTypeName<T>(this List<T> list, string typeName)
        {
            return list.Find(el => el.GetType().Name.Equals(typeName));
        }

        public static T GetElementByType<T>(this List<T> list, Type type)
        {
            return list.Find(el => el.GetType() == type);
        }

        public static T PopAt<T>(this List<T> list, int index)
        {
            var element = list[index];
            list.RemoveAt(index);
            return element;
        }

        public static T Random<T>(this List<T> list)
        {
            var random = new Random();
            var index = random.Next(list.Count);
            return list[index];
        }
    }

}
