using System.Collections.Generic;

namespace Common.HexField.HexGridObject
{
    public class GridObjectEqualityComparer : IEqualityComparer<GridObject>
    {
        public bool Equals(GridObject x, GridObject y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            return x.GetType() == y.GetType() && x.Equals(y);
        }

        public int GetHashCode(GridObject obj)
        {
            return obj.GetHashCode();
        }
    }
}
