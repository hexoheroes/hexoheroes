using Common.HexField.HexGridObject;
using Common.HexField.HexPosition;
using Common.Utils;

namespace Common.HexField.HexGrid
{
    public static class GridUtils
    {
        private const float GridObjectsDistanceThreshold = 0.01f;

        public static PositionInt CalcNewGridObjectPointyPosition(GridObject gridObject, GridObject fromGridObject, PositionInt fromGridObjectPosition)
        {
            var isEqualByX = gridObject.GetPosition().EqualsByX(fromGridObject.GetPosition(), GridObjectsDistanceThreshold);
            var isEqualByZ = gridObject.GetPosition().EqualsByZ(fromGridObject.GetPosition(), GridObjectsDistanceThreshold);
            
            return isEqualByZ switch
            {
                -1 when isEqualByX == -1 => fromGridObjectPosition.GetNeighborPointyGridPosition(1),
                0 when isEqualByX == -1 => fromGridObjectPosition.GetNeighborPointyGridPosition(2),
                1 when isEqualByX == -1 => fromGridObjectPosition.GetNeighborPointyGridPosition(3),
                1 when isEqualByX == 1 => fromGridObjectPosition.GetNeighborPointyGridPosition(4),
                0 when isEqualByX == 1 => fromGridObjectPosition.GetNeighborPointyGridPosition(5),
                -1 when isEqualByX == 1 => fromGridObjectPosition.GetNeighborPointyGridPosition(6),
                _ => new PositionInt(0, 0, 0)
            };
        }
        
        public static bool DistanceBetweenGridObjectEqual(GridObject object1, GridObject object2, float distance)
        {
            return Vector3Utils.DistanceBetweenVectorsEqual(object1.GetPosition().RemoveYAxis(), object2.GetPosition().RemoveYAxis(),
                distance, GridObjectsDistanceThreshold);
        }
    }
}
