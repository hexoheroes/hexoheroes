using System;
using System.Collections.Generic;
using System.Linq;
using Common.HexField.HexGridObject;
using Common.HexField.HexList;

namespace Common.HexField.HexGrid
{
    // https://www.redblobgames.com/grids/hexagons/
    // pointy
    public class PointyGrid
    {
        private readonly Grid _grid;

        public PointyGrid(IEnumerable<GridObject> gridObjects)
        {
            _grid = new PointyGridCreator(gridObjects).Create();
        }
        
        public int Distance(GridObject from, GridObject to, int maxDistance)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.Distance(to);
        }
        
        public IEnumerable<GridObject> GetLine(GridObject from, GridObject to, int maxDistance)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetLine(to);
        }
        
        public IEnumerable<GridObject> GetAllObjects(GridObject from, int maxDistance)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetAllObjects();
        }
        
        public IEnumerable<GridObject> GetAvailableObjects(GridObject from, int maxDistance, IEnumerable<GridObject> obstacles)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetAvailableObjects(obstacles);
        }
        
        public IEnumerable<GridObject> GetObstacleObjects(GridObject from, int maxDistance, IEnumerable<GridObject> obstacles)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetObstacleObjects(obstacles);
        }
        
        public IEnumerable<GridObject> GetReachableObjects(GridObject from, int maxDistance, GridObjectWeights weights)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetReachableObjects(weights);
        }
        
        public IEnumerable<GridObject> GetPath(GridObject from, GridObject to, int maxDistance, GridObjectWeights weights)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetPath(to, weights);
        }
        
        public int GetPathTotalMovements(GridObject from, GridObject to, int maxDistance, GridObjectWeights weights)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetPathTotalMovements(to, weights);
        }
        
        public Dictionary<GridObject, int> GetPathWithMovements(GridObject from, GridObject to, int maxDistance, GridObjectWeights weights)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetPathWithMovements(to, weights);
        }
        
        public IEnumerable<GridObject> GetFieldOfView(GridObject from, int maxDistance, int fromHeight, GridObjectHeights heights)
        {
            var range = new GridRangeCreator(_grid).Create(from, maxDistance);
            return range.GetFieldOfView(fromHeight, heights);
        }
    }
}