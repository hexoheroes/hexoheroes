using Common.HexField.HexGridObject;
using Common.HexField.HexGridRange;
using Common.HexField.HexPosition;
using UnityEngine;


namespace Common.HexField.HexGrid
{
    public class GridRangeCreator
    {
        private readonly Grid _grid;

        public GridRangeCreator(Grid grid)
        {
            _grid = grid;
        }

        public GridRange Create(GridObject centerGridObject, int distance)
        {
            return _grid.GridObjectExists(centerGridObject) ? GetRange(centerGridObject, distance) : new GridRangeEmpty();
        }
        
        private GridRange GetRange(GridObject center, int distance)
        {
            var range = new Grid();
            var centerPosition = _grid.FindGridObjectPositionEx(center);
        
            for (var x = -distance; x <= distance; x++)
            {
                for (var y = Mathf.Max(-distance, -x - distance); y <= Mathf.Min(distance, -x + distance); y++)
                {
                    var z = -x - y;
                    var position = new PositionInt(x, y, z);
                    
                    var positionOffset = PositionIntUtils.Sum(centerPosition, position);
                    var gridObject = _grid.FindGridObjectByPosition(positionOffset);
                    if (gridObject != null)
                    {
                        range.Add(gridObject, positionOffset);
                    }
                }
            }
        
            return new GridRange(range, center, distance);
        }
    }
}
