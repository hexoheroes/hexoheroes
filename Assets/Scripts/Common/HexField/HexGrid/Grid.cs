using System.Collections.Generic;
using System.Linq;
using Common.HexField.Exceptions;
using Common.HexField.HexGridObject;
using Common.HexField.HexPosition;

namespace Common.HexField.HexGrid
{
    public class Grid
    {
        private readonly Dictionary<GridObject, PositionInt> _gridObjects;
        private readonly Dictionary<PositionInt, GridObject> _gridPositions;

        public Grid()
        {
            _gridObjects = new Dictionary<GridObject, PositionInt>(new GridObjectEqualityComparer());
            _gridPositions = new Dictionary<PositionInt, GridObject>(new PositionIntEqualityComparer());
        }

        public int Count()
        {
            return _gridObjects.Count;
        }

        public void Add(GridObject newObject, PositionInt position)
        {
            if (GridObjectExists(newObject))
            {
                throw new GridObjectAlreadyAddedException(newObject);
            }

            if (GridObjectExistsByPosition(position))
            {
                throw new GridObjectAlreadyAddedException(position);
            }
            
            _gridObjects.Add(newObject, position);
            _gridPositions.Add(position, newObject);
        }

        public IEnumerable<GridObject> GetObjects()
        {
            return _gridObjects.Keys;
        }
        
        public GridObject FindGridObjectByPosition(PositionInt position)
        {
            _gridPositions.TryGetValue(position, out var obj);
            return obj ? obj : null;
        }
        
        public GridObject FindGridObjectByPosition(PositionFloat position)
        {
            return FindGridObjectByPosition(position.ToIntPosition());
        }
        
        public GridObject FindGridObjectByPositionEx(PositionInt position)
        {
            _gridPositions.TryGetValue(position, out var obj);
            return obj ? obj : throw new GridObjectMissingException(position);
        }
        
        public PositionInt FindGridObjectPosition(GridObject obj)
        {
            return _gridObjects.ContainsKey(obj) ? _gridObjects[obj] : PositionInt.NaN();
        }
        
        public PositionInt FindGridObjectPositionEx(GridObject obj)
        {
            return _gridObjects.ContainsKey(obj) ? _gridObjects[obj] : throw new GridObjectMissingException(obj);
        }
        
        public bool GridObjectExists(GridObject obj)
        {
            return _gridObjects.ContainsKey(obj);
        }
        
        public bool GridObjectExistsByPosition(PositionInt position)
        {
            return _gridPositions.ContainsKey(position);
        }
        
        public int Distance(GridObject from, GridObject to)
        {
            return PositionIntUtils.Distance(_gridObjects[from], _gridObjects[to]);
        }
        
        public PositionFloat Lerp(GridObject from, GridObject to, float t)
        {
            return PositionIntUtils.Lerp(_gridObjects[from], _gridObjects[to], t);
        }
        
        public bool AreGridObjectsNeighbors(GridObject obj1, GridObject obj2)
        {
            var position1 = _gridObjects[obj1];
            var position2 = _gridObjects[obj2];

            return PositionIntUtils.Distance(position1, position2) == 1;
        }
    }
}
