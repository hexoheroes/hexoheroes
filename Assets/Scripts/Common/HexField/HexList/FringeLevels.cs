using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.HexField.HexGridObject;
using Common.Utils;
using UnityEngine;

namespace Common.HexField.HexList
{
    public class FringeLevels
    {
        private readonly Dictionary<GridObject, int> _levels;

        public FringeLevels()
        {
            _levels = new Dictionary<GridObject, int>(new GridObjectEqualityComparer());
        }

        public void Add(GridObject obj, int level)
        {
            _levels.Add(obj, level);
        }

        public IEnumerable<GridObject> GetAllObjects()
        {
            return _levels.Keys;
        }

        public IEnumerable<GridObject> GetObjects(int level)
        {
            return _levels.FindKeysByValue(level);
        }

        public bool ObjectExists(GridObject obj)
        {
            return _levels.ContainsKey(obj);
        }
        
        public int GetLevel(GridObject obj)
        {
            return _levels[obj];
        }

        public Dictionary<GridObject, int> GetLevels()
        {
            return _levels;
        }
    }
}
