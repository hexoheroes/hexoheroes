using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.HexField.HexGridObject;
using Common.Utils;
using UnityEngine;

namespace Common.HexField.HexList
{
    public class GridObjectWeights
    {
        private readonly Dictionary<Type, int> _weights;
        public const int BadWeight = -1;

        public GridObjectWeights()
        {
            _weights = new Dictionary<Type, int>();
        }

        public void Add(GridObject obj, int weight)
        {
            _weights.Add(obj.GetType(), weight);
        }

        public int GetWeight(GridObject obj)
        {
            return _weights.ContainsKey(obj.GetType()) ? _weights[obj.GetType()] : BadWeight;
        }
    }
}
