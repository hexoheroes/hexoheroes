using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.HexField.HexGridObject;
using Common.Utils;
using UnityEngine;

namespace Common.HexField.HexList
{
    public class GridObjectHeights
    {
        private readonly Dictionary<Type, int> _heights;
        public const int BadHeight = -1;
        public const int Infinite = int.MaxValue;

        public GridObjectHeights()
        {
            _heights = new Dictionary<Type, int>();
        }

        public void Add(GridObject obj, int height)
        {
            _heights.Add(obj.GetType(), height);
        }

        public int GetHeight(GridObject obj)
        {
            return _heights.ContainsKey(obj.GetType()) ? _heights[obj.GetType()] : BadHeight;
        }
    }
}