using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Common.HexField.HexGridObject;
using Common.HexField.HexList;
using Common.HexField.HexPosition;
using Common.Utils;
using UnityEngine;
using Grid = Common.HexField.HexGrid.Grid;

namespace Common.HexField.HexGridRange
{
    public class GridRange
    {
        private readonly Grid _grid;
        private readonly GridObject _center;
        private readonly int _distance;

        public GridRange(Grid grid, GridObject center, int distance)
        {
            _grid = grid;
            _center = center;
            _distance = distance;
        }
        
        public virtual int Distance(GridObject to)
        {
            return _grid.GridObjectExists(to) ? _grid.Distance(_center, to) : 0;
        }
        
        public IEnumerable<GridObject> GetLine(GridObject to)
        {
            return _grid.GridObjectExists(to)
                ? FindGridObjectsOnLine(_center, to).GetAllObjects()
                : Enumerable.Empty<GridObject>();
        }
        
        public IEnumerable<GridObject> GetAllObjects()
        {
            return _grid.GetObjects();
        }
        
        public IEnumerable<GridObject> GetAvailableObjects(IEnumerable<GridObject> obstacles)
        {
            return from obj in _grid.GetObjects() where !obstacles.Any(obj.EqualsByType) select obj;
        }
        
        public IEnumerable<GridObject> GetObstacleObjects(IEnumerable<GridObject> obstacles)
        {
            return from obj in _grid.GetObjects() where obstacles.Any(obj.EqualsByType) select obj;
        }
        
        public IEnumerable<GridObject> GetReachableObjects(GridObjectWeights weights)
        {
            var fringeLevels = GetFringeLevels(weights);
            return fringeLevels.GetAllObjects();
        }
        
        public Dictionary<GridObject, int> GetReachableObjectsWithMovements(GridObjectWeights weights)
        {
            var fringeLevels = GetFringeLevels(weights);
            return fringeLevels.GetLevels();
        }

        public IEnumerable<GridObject> GetPath(GridObject to, GridObjectWeights weights)
        {
            var pathWithMovements = GetPathWithMovements(to, weights);
            return pathWithMovements.OrderBy(key => key.Value).Select(pathItem => pathItem.Key);
        }
        
        public int GetPathTotalMovements(GridObject to, GridObjectWeights weights)
        {
            var pathWithMovements = GetPathWithMovements(to, weights);
            return pathWithMovements.Values.Max();
        }
        
        public Dictionary<GridObject, int> GetPathWithMovements(GridObject to, GridObjectWeights weights)
        {
            if (_grid.GridObjectExists(to) == false || new GridObjectEqualityComparer().Equals(_center, to)) return new Dictionary<GridObject, int>();
            
            var fringeLevels = GetFringeLevels(weights);
            if (fringeLevels.ObjectExists(to) == false) return new Dictionary<GridObject, int>();
            
            var toLevel = fringeLevels.GetLevel(to);

            var path = new Dictionary<GridObject, int>();
            path.Add(to, toLevel);
            var previousGridObject = to;
            for (var levelInd = toLevel - 1; levelInd > 0; levelInd--)
            {
                var levelGridObjects = fringeLevels.GetObjects(levelInd);
                foreach (var levelGridObject in levelGridObjects)
                {
                    if (_grid.AreGridObjectsNeighbors(levelGridObject, previousGridObject) == false) continue;
                    path.Add(levelGridObject, levelInd);
                    previousGridObject = levelGridObject;
                    break;
                }
            }
            
            path.Add(_center, 0);
            return path;
        }

        public IEnumerable<GridObject> GetFieldOfView(int fromHeight, GridObjectHeights heights)
        {
            var visitedObjects = new HashSet<GridObject>(new GridObjectEqualityComparer());
            var fringesLevels = new FringeLevels();
            var visibleGridObjects = new List<GridObject>();
            var invisibleGridObjects = new List<GridObject>();
            
            fringesLevels.Add(_center, 0);
            visibleGridObjects.Add(_center);
            visitedObjects.Add(_center);
            
            for (var step = 0; step < _distance; step++)
            {
                var levelObjects = fringesLevels.GetObjects(step).ToList();
                for (var levelObjInd = 0; levelObjInd < levelObjects.Count; levelObjInd++)
                {
                    for (var neighborDir = 1; neighborDir <= 6; neighborDir++)
                    {
                        var neighborGridObject = GetObjectNeighbor(levelObjects[levelObjInd], neighborDir);
                        if (neighborGridObject == null || visitedObjects.Contains(neighborGridObject)) continue;
                        visitedObjects.Add(neighborGridObject);
                        fringesLevels.Add(neighborGridObject, step + 1);
                        
                        var height = heights.GetHeight(neighborGridObject);
                        if (height == GridObjectHeights.BadHeight)
                        {
                            Debug.Log($"Found Bad Height. Target object [{neighborGridObject}]");
                            invisibleGridObjects.Add(neighborGridObject);
                            continue;
                        }
            
                        if (height > fromHeight)
                        {
                            invisibleGridObjects.Add(neighborGridObject);
                        }
                        else
                        {
                            var gridObjectsLine = FindGridObjectsOnLine(_center, neighborGridObject);
                            var areAllObjectsVisible = !gridObjectsLine.GetAllObjects().Any(obj => invisibleGridObjects.Contains(obj));
                            if (areAllObjectsVisible)
                            {
                                visibleGridObjects.Add(neighborGridObject);
                            }
                            else
                            {
                                invisibleGridObjects.Add(neighborGridObject);
                            }
                        }
                    }
                }
            }
            
            
            return visibleGridObjects;
        }

        private FringeLevels GetFringeLevels(GridObjectWeights weights)
        {
            var visitedObjects = new HashSet<GridObject>(new GridObjectEqualityComparer());
            var fringesLevels = new FringeLevels();
        
            fringesLevels.Add(_center, 0);
            visitedObjects.Add(_center);
        
            for (var step = 0; step < _distance; step++)
            {
                var levelObjects = fringesLevels.GetObjects(step).ToList();
                for (var levelObjInd = 0; levelObjInd < levelObjects.Count; levelObjInd++)
                {
                    for (var neighborDir = 1; neighborDir <= 6; neighborDir++)
                    {
                        var neighborGridObject = GetObjectNeighbor(levelObjects[levelObjInd], neighborDir);
                        if (neighborGridObject == null || visitedObjects.Contains(neighborGridObject)) continue;
                        visitedObjects.Add(neighborGridObject);
                        
                        var weight = weights.GetWeight(neighborGridObject);
                        if (weight == GridObjectWeights.BadWeight)
                        {
                            Debug.Log($"Found Bad Weight. Target object [{neighborGridObject}]");
                            continue;
                        }
                        if (step + weight > _distance)
                        {
                            continue;
                        }
                        
                        fringesLevels.Add(neighborGridObject, step + weight);
                    }
                }
            }
        
            return fringesLevels;
        }
        
        private GridObject GetObjectNeighbor(GridObject from, int direction)
        {
            var fromPosition = _grid.FindGridObjectPosition(from);
            if (PositionInt.IsNaN(fromPosition)) return null;
            var neighborPosition = fromPosition.GetNeighborPointyGridPosition(direction);
            return _grid.FindGridObjectByPosition(neighborPosition);
        }

        private FringeLevels FindGridObjectsOnLine(GridObject from, GridObject to)
        {
            var distance = _grid.Distance(from, to);
            var epsilon1 = new PositionFloat(0.0001f, 0.0001f, 0.0001f);
            var epsilon2 = new PositionFloat(-0.0001f, -0.0001f, -0.0001f);
            
            var result = new FringeLevels();
            for (var i = 0; i <= distance; i++)
            {
                var lerpPosition = _grid.Lerp(from, to, 1.0f / distance * i);
                var position1 = PositionFloatUtils.Sum(lerpPosition, epsilon1);
                var position2 = PositionFloatUtils.Sum(lerpPosition, epsilon2);
                var gridObj1 = _grid.FindGridObjectByPosition(position1);
                var gridObj2 = _grid.FindGridObjectByPosition(position2);
                
                if (new GridObjectEqualityComparer().Equals(gridObj1, gridObj2))
                {
                    result.Add(gridObj1, i);
                    continue;
                }
                
                if (gridObj1 == null && gridObj2 == null) continue;
                
                if (gridObj1 == null && gridObj2)
                {
                    result.Add(gridObj2, i);
                    continue;
                }
                
                if (gridObj1 && gridObj2 == null)
                {
                    result.Add(gridObj1, i);
                    continue;
                }
                
                result.Add(gridObj1, i);
                result.Add(gridObj2, i);
            }
        
            return result;
        }
    }
}
