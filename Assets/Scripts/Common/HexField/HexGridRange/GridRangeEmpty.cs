using System.Collections;
using System.Collections.Generic;
using Common.HexField.HexGrid;
using Common.HexField.HexGridObject;

namespace Common.HexField.HexGridRange
{
    public class GridRangeEmpty : GridRange
    {
        public GridRangeEmpty() : base(new Grid(), null, 0)
        {
        }

        public override int Distance(GridObject to)
        {
            return 0;
        }
    }
}
