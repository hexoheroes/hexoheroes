using System;
using System.Runtime.Serialization;
using Common.HexField.HexGridObject;
using Common.HexField.HexPosition;

namespace Common.HexField.Exceptions
{
    [Serializable]
    public class GridObjectAlreadyAddedException : Exception
    {
        public GridObjectAlreadyAddedException(GridObject obj) : base($"Grid object [{obj}] already added")
        {

        }
        
        public GridObjectAlreadyAddedException(PositionInt position) : base($"Grid object already added to position [{position}]")
        {

        }

        public GridObjectAlreadyAddedException(string message) : base(message)
        {

        }

        public GridObjectAlreadyAddedException(string message, Exception inner) : base(message, inner)
        {

        }
        
        protected GridObjectAlreadyAddedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}
