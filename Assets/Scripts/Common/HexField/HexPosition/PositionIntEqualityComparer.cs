using System.Collections;
using System.Collections.Generic;
using Common.HexField.HexPosition;
using UnityEngine;

namespace Common.HexField.HexPosition
{
    public class PositionIntEqualityComparer : IEqualityComparer<PositionInt>
    {
        public bool Equals(PositionInt x, PositionInt y)
        {
            return x.X == y.X && x.Y == y.Y && x.Z == y.Z;
        }

        public int GetHashCode(PositionInt obj)
        {
            unchecked
            {
                var hashCode = obj.X;
                hashCode = (hashCode * 397) ^ obj.Y;
                hashCode = (hashCode * 397) ^ obj.Z;
                return hashCode;
            }
        }
    }
}
