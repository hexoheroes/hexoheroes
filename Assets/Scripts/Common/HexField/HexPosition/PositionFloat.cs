using System;
using Common.Utils;

namespace Common.HexField.HexPosition
{
    public readonly struct PositionFloat
    {
        public float X { get; }
        public float Y { get; }
        public float Z { get; }

        public PositionFloat(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return $"{X};{Y};{Z}";
        }
    }
}
