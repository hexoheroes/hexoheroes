using System;

namespace Common.HexField.HexPosition
{
    public readonly struct PositionInt
    {
        public int X { get; }
        public int Y { get; }
        public int Z { get; }

        public PositionInt(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return $"{X};{Y};{Z}";
        }

        public static bool IsNaN(PositionInt value)
        {
            return new PositionIntComparator(NaN(), value).Equals();
        }

        public static PositionInt NaN()
        {
            return new PositionInt(int.MaxValue, int.MaxValue, int.MaxValue);
        }
    }
}
