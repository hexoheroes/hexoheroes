using Common.Utils;

namespace Common.HexField.HexPosition
{
    public class PositionFloatComparator
    {
        private readonly PositionFloat _pos1;
        private readonly PositionFloat _pos2;

        public PositionFloatComparator(PositionFloat pos1, PositionFloat pos2)
        {
            _pos1 = pos1;
            _pos2 = pos2;
        }
        
        public bool Equals()
        {
            return FloatUtils.EqualsApproximately(_pos1.X, _pos2.X, 0.0001f) 
                   && FloatUtils.EqualsApproximately(_pos1.Y, _pos2.Y, 0.0001f) 
                   && FloatUtils.EqualsApproximately(_pos1.Z, _pos2.Z, 0.0001f);
        }
    }
}
